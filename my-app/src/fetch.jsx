import {useEffect} from "react";

const Fetch = () => {
    useEffect(() => {
        fetchRequest().then()
    }, [])

    const fetchRequest = async () => {
        await fetch("http://localhost:3001/counter", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({random_num: Math.random()})
        }).then((response) => {
            return response.json();
        })
            .then((data) => {
                console.log(data);
            });
    }

    return <>
        Hello again
    </>
}

export default Fetch;
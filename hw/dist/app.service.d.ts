import { CountEntity } from "./entities/Count.entity";
import { Repository } from "typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
export declare class AppService extends TypeOrmCrudService<CountEntity> {
    private readonly repository;
    constructor(repository: Repository<CountEntity>);
    createCount(random_num: number): Promise<CountEntity>;
}

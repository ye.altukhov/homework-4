import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { MysqlHost, MysqlPort } from './types';
export declare class MysqlConfigService implements TypeOrmOptionsFactory {
    private readonly configService;
    constructor(configService: ConfigService);
    get host(): MysqlHost;
    get port(): MysqlPort;
    get user(): string;
    get password(): string;
    get database(): string;
    createTypeOrmOptions(): TypeOrmModuleOptions;
}

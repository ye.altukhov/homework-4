"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configuration = exports.TOKEN = void 0;
const config_1 = require("@nestjs/config");
exports.TOKEN = 'mysql';
exports.configuration = (0, config_1.registerAs)(exports.TOKEN, () => ({
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
}));
//# sourceMappingURL=configuration.js.map
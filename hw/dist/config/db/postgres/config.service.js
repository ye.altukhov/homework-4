"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MysqlConfigService = void 0;
const path_1 = require("path");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const configuration_1 = require("./configuration");
const ENTITIES_PATTERN = '**/*.entity{.ts,.js}';
const MIGRATIONS_PATTERN = 'migrations/*{.ts,.js}';
let MysqlConfigService = class MysqlConfigService {
    constructor(configService) {
        this.configService = configService;
    }
    get host() {
        return this.configService.get(`${configuration_1.TOKEN}.host`);
    }
    get port() {
        return Number(this.configService.get(`${configuration_1.TOKEN}.port`));
    }
    get user() {
        return this.configService.get(`${configuration_1.TOKEN}.user`);
    }
    get password() {
        return this.configService.get(`${configuration_1.TOKEN}.password`);
    }
    get database() {
        return this.configService.get(`${configuration_1.TOKEN}.database`);
    }
    createTypeOrmOptions() {
        const entities = (0, path_1.join)(__dirname, '..', '..', '..', ENTITIES_PATTERN);
        const migrations = (0, path_1.join)(__dirname, '..', '..', '..', MIGRATIONS_PATTERN);
        return {
            type: 'mysql',
            dropSchema: false,
            migrationsRun: true,
            synchronize: true,
            host: this.host,
            port: this.port,
            username: this.user,
            password: this.password,
            database: this.database,
            entities: [entities],
            migrations: [
                "/Users/e-altukhov/Desktop/pawa/adlightNewDesign/hw/src/migrations/**/*{.js,.ts}",
            ],
            logging: true,
            logger: 'file',
        };
    }
};
MysqlConfigService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], MysqlConfigService);
exports.MysqlConfigService = MysqlConfigService;
//# sourceMappingURL=config.service.js.map
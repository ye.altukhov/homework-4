export declare const TOKEN = "mysql";
export declare const configuration: (() => {
    host: string;
    port: string;
    user: string;
    password: string;
    database: string;
}) & import("@nestjs/config").ConfigFactoryKeyHost<{
    host: string;
    port: string;
    user: string;
    password: string;
    database: string;
}>;

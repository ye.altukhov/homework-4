export declare type MysqlHost = 'localhost' | string;
export declare type MysqlPort = 3306 | number;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppConfigService = void 0;
const path_1 = require("path");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const configuration_1 = require("./configuration");
let AppConfigService = class AppConfigService {
    constructor(configService) {
        this.configService = configService;
    }
    get port() {
        return Number(this.configService.get(`${configuration_1.TOKEN}.port`));
    }
    get jwtSecret() {
        return this.configService.get(`${configuration_1.TOKEN}.jwtSecret`);
    }
    get webAdminUrl() {
        return this.configService.get(`${configuration_1.TOKEN}.webAdminUrl`);
    }
    get managerAdminUrl() {
        return this.configService.get(`${configuration_1.TOKEN}.managerAdminUrl`);
    }
    get uploadsFolderName() {
        return this.configService.get(`${configuration_1.TOKEN}.uploadsFolderName`);
    }
    get blackIpsForClicks() {
        return this.configService.get(`${configuration_1.TOKEN}.blackIpsForClicks`);
    }
    get geoEndpoint() {
        return this.configService.get(`${configuration_1.TOKEN}.geoEndpoint`);
    }
    get refCoef() {
        return Number(this.configService.get(`${configuration_1.TOKEN}.refCoef`));
    }
    get psApiKey() {
        return this.configService.get(`${configuration_1.TOKEN}.psApiKey`);
    }
    createLoggerOptions() {
        return [
            {
                rootPath: (0, path_1.join)(__dirname, '..', '..', '..', this.uploadsFolderName),
                serveRoot: `/${this.uploadsFolderName}`,
            },
            {
                rootPath: (0, path_1.join)(__dirname, '..', '..', 'client-manager/build'),
            },
        ];
    }
};
AppConfigService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], AppConfigService);
exports.AppConfigService = AppConfigService;
//# sourceMappingURL=config.service.js.map
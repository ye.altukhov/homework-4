export declare const TOKEN = "app";
export declare const configuration: (() => {
    port: string;
    jwtSecret: string;
}) & import("@nestjs/config").ConfigFactoryKeyHost<{
    port: string;
    jwtSecret: string;
}>;

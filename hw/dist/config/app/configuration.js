"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configuration = exports.TOKEN = void 0;
const config_1 = require("@nestjs/config");
exports.TOKEN = 'app';
exports.configuration = (0, config_1.registerAs)(exports.TOKEN, () => ({
    port: process.env.SERVER_PORT,
    jwtSecret: process.env.SERVER_JWT_SECRET,
}));
//# sourceMappingURL=configuration.js.map
import { ConfigService } from '@nestjs/config';
import { ServeStaticModuleOptions, ServeStaticModuleOptionsFactory } from '@nestjs/serve-static';
import { Port } from './types';
export declare class AppConfigService implements ServeStaticModuleOptionsFactory {
    private readonly configService;
    constructor(configService: ConfigService);
    get port(): Port;
    get jwtSecret(): string;
    get webAdminUrl(): string;
    get managerAdminUrl(): string;
    get uploadsFolderName(): string;
    get blackIpsForClicks(): string | undefined;
    get geoEndpoint(): string;
    get refCoef(): number;
    get psApiKey(): string;
    createLoggerOptions(): ServeStaticModuleOptions[];
}

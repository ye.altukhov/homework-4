"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppDataSource = void 0;
const typeorm_1 = require("typeorm");
const path_1 = require("path");
const ENTITIES_PATTERN = "entities/*.entity.ts";
const entities = (0, path_1.join)(__dirname, ENTITIES_PATTERN);
exports.AppDataSource = new typeorm_1.DataSource({
    type: "mysql",
    dropSchema: false,
    migrationsRun: true,
    synchronize: true,
    host: process.env.MYSQL_HOST,
    port: +process.env.MYSQL_PORT,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    entities: ["src/entities/*.entity{.ts,.js}"],
    migrations: [
        "/Users/e-altukhov/Desktop/pawa/adlightNewDesign/server/src/migrations/**/*{.js,.ts}",
    ],
    logging: true,
    logger: "file",
});
//# sourceMappingURL=data-source.js.map
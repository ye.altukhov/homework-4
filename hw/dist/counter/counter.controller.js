"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CounterController = void 0;
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const Count_entity_1 = require("../entities/Count.entity");
const counter_service_1 = require("./counter.service");
let CounterController = class CounterController {
    constructor(service) {
        this.service = service;
    }
};
CounterController = __decorate([
    (0, crud_1.Crud)({
        model: {
            type: Count_entity_1.CountEntity
        }
    }),
    (0, common_1.Controller)('counter'),
    __metadata("design:paramtypes", [counter_service_1.CounterService])
], CounterController);
exports.CounterController = CounterController;
//# sourceMappingURL=counter.controller.js.map
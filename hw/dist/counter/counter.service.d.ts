import { CountEntity } from "../entities/Count.entity";
import { Repository } from "typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
export declare class CounterService extends TypeOrmCrudService<CountEntity> {
    private readonly repository;
    constructor(repository: Repository<CountEntity>);
}

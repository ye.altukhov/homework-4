import { AppService } from './app.service';
import { CountEntity } from "./entities/Count.entity";
export declare class AppController {
    private readonly service;
    constructor(service: AppService);
    createCount(): Promise<CountEntity>;
}

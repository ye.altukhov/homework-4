import {Controller, Get, Post} from '@nestjs/common';
import { Crud } from "@nestjsx/crud";

import { AppService } from './app.service';
import {CountEntity} from "./entities/Count.entity";

@Crud({
  model: {
    type: CountEntity
  }
})
@Controller()
export class AppController {
  constructor(private readonly service: AppService) {}


  @Post("/")
  createCount(): Promise<CountEntity> {
    return this.service.createCount(
      Math.random()
    )
  }
}

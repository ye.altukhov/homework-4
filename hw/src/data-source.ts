import { DataSource } from "typeorm";
import { join } from "path";
const ENTITIES_PATTERN = "entities/*.entity.ts";

const entities = join(__dirname, ENTITIES_PATTERN);

export const AppDataSource = new DataSource({
  type: "mysql",
  dropSchema: false,
  migrationsRun: true,
  synchronize: true,
  host: process.env.MYSQL_HOST,
  port: +process.env.MYSQL_PORT,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  // entities: [entities],
  entities: ["src/entities/*.entity{.ts,.js}"],
  migrations: [
    "/Users/e-altukhov/Desktop/pawa/adlightNewDesign/server/src/migrations/**/*{.js,.ts}",
  ],
  logging: true,
  logger: "file",
});

import { registerAs } from '@nestjs/config';

export const TOKEN = 'app';

export const configuration = registerAs(TOKEN, () => ({
  port: process.env.SERVER_PORT,
  jwtSecret: process.env.SERVER_JWT_SECRET,
}));

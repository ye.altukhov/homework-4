import { join } from 'path';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ServeStaticModuleOptions,
  ServeStaticModuleOptionsFactory,
} from '@nestjs/serve-static';
import { Port } from './types';
import { TOKEN } from './configuration';

@Injectable()
export class AppConfigService implements ServeStaticModuleOptionsFactory {
  constructor(private readonly configService: ConfigService) {}

  get port(): Port {
    return Number(this.configService.get<Port>(`${TOKEN}.port`));
  }

  get jwtSecret(): string {
    return this.configService.get<string>(`${TOKEN}.jwtSecret`);
  }

  get webAdminUrl(): string {
    return this.configService.get<string>(`${TOKEN}.webAdminUrl`);
  }

  get managerAdminUrl(): string {
    return this.configService.get<string>(`${TOKEN}.managerAdminUrl`);
  }

  get uploadsFolderName(): string {
    return this.configService.get<string>(`${TOKEN}.uploadsFolderName`);
  }

  get blackIpsForClicks(): string | undefined {
    return this.configService.get<string>(`${TOKEN}.blackIpsForClicks`);
  }

  get geoEndpoint(): string {
    return this.configService.get<string>(`${TOKEN}.geoEndpoint`);
  }

  get refCoef(): number {
    return Number(this.configService.get<number>(`${TOKEN}.refCoef`));
  }

  get psApiKey(): string {
    return this.configService.get<string>(`${TOKEN}.psApiKey`);
  }

  createLoggerOptions(): ServeStaticModuleOptions[] {
    return [
      {
        rootPath: join(__dirname, '..', '..', '..', this.uploadsFolderName),
        serveRoot: `/${this.uploadsFolderName}`,
      },
      {
        rootPath: join(__dirname, '..', '..', 'client-manager/build'),
      },
    ];
  }
}

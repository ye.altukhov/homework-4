export type MysqlHost = 'localhost' | string;

export type MysqlPort = 3306 | number;

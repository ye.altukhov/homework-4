import { registerAs } from '@nestjs/config';

export const TOKEN = 'mysql';

export const configuration = registerAs(TOKEN, () => ({
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
}));

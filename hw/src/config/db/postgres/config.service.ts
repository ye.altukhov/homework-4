import {join} from 'path';
import {Injectable} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {TypeOrmModuleOptions, TypeOrmOptionsFactory} from '@nestjs/typeorm';
import {TOKEN} from './configuration';
import {MysqlHost, MysqlPort} from './types';

const ENTITIES_PATTERN = '**/*.entity{.ts,.js}';
const MIGRATIONS_PATTERN = 'migrations/*{.ts,.js}';

@Injectable()
export class MysqlConfigService implements TypeOrmOptionsFactory {
    constructor(private readonly configService: ConfigService) {
    }

    get host(): MysqlHost {
        return this.configService.get<MysqlHost>(`${TOKEN}.host`);
    }

    get port(): MysqlPort {
        return Number(this.configService.get<MysqlPort>(`${TOKEN}.port`));
    }

    get user(): string {
        return this.configService.get<string>(`${TOKEN}.user`);
    }

    get password(): string {
        return this.configService.get<string>(`${TOKEN}.password`);
    }

    get database(): string {
        return this.configService.get<string>(`${TOKEN}.database`);
    }

    createTypeOrmOptions(): TypeOrmModuleOptions {
        const entities = join(__dirname, '..', '..', '..', ENTITIES_PATTERN);
        const migrations = join(__dirname, '..', '..', '..', MIGRATIONS_PATTERN);

        return {
            type: 'mysql',
            dropSchema: false,
            migrationsRun: true,
            synchronize: true,
            host: this.host,
            port: this.port,
            username: this.user,
            password: this.password,
            database: this.database,
            entities: [entities],
            migrations: [
                "/Users/e-altukhov/Desktop/pawa/adlightNewDesign/hw/src/migrations/**/*{.js,.ts}",
            ],
            logging: true,
            logger: 'file',
        };
    }
}

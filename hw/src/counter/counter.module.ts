import {Module} from '@nestjs/common';
import {CounterService} from './counter.service';
import {CounterController} from './counter.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CountEntity} from "../entities/Count.entity";

@Module({
    imports: [TypeOrmModule.forFeature([CountEntity]),
    ],
    providers: [CounterService],
    controllers: [CounterController]
})
export class CounterModule {
}

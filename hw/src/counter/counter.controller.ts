import { Controller } from '@nestjs/common';
import {Crud} from "@nestjsx/crud";
import {CountEntity} from "../entities/Count.entity";
import {CounterService} from "./counter.service";

@Crud({
    model: {
        type: CountEntity
    }
})
@Controller('counter')
export class CounterController {
    constructor(private readonly service: CounterService) {
    }
}

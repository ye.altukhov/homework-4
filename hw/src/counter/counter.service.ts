import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {CountEntity} from "../entities/Count.entity";
import {Repository} from "typeorm";
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";

@Injectable()
export class CounterService extends TypeOrmCrudService<CountEntity> {
    constructor(
        @InjectRepository(CountEntity)
        private readonly repository: Repository<CountEntity>
    ) {
        super(repository)
    }
}

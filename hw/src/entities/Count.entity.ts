import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class CountEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    random_num: number
}
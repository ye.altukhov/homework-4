import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {MysqlConfigModule} from "./config/db/postgres/config.module";
import {MysqlConfigService} from "./config/db/postgres/config.service";
import {CountEntity} from "./entities/Count.entity";
import { CounterModule } from './counter/counter.module';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            imports: [MysqlConfigModule],
            useExisting: MysqlConfigService,
        }),
        TypeOrmModule.forFeature([CountEntity]),
        CounterModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}

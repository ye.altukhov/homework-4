import { Injectable } from '@nestjs/common';
import {CountEntity} from "./entities/Count.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";

@Injectable()
export class AppService extends TypeOrmCrudService<CountEntity> {
  constructor(
      @InjectRepository(CountEntity)
      private readonly repository: Repository<CountEntity>
  ) {
    super(repository)
  }

  async createCount(random_num: number): Promise<CountEntity> {
    return await this.repository.create({
      random_num
    });
  }
}
